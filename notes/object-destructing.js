// object destructing => is going to look at the start state object. 
// It would then pull off the resource property because we have the curly braces right
// here and assign that resource value to the variable of resource.
const {resource} = this.state;