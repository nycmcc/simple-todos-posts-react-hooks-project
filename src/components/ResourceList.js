import React, {useState, useEffect} from 'react';
import axios from 'axios';

const ResourceList = ({ resource }) => { // resource = prop
  const [resources, setResources] = useState([]); // this.state = [];
  useEffect(
    () => {
      (async resource => {
        const response = await axios.get(
          `https://jsonplaceholder.typicode.com/${resource}`
        );

        setResources(response.data);
      })(resource); // immediately invoke with resource
    }, 
    [resource]
  ); 

  return (
    <ul>
      {resources.map(record => <li key={record.id}>{record.title}</li>)}
    </ul>
  );
}

export default ResourceList;