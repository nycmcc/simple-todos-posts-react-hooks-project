import React, {useState} from 'react';
import ResourceList from './ResourceList';

const App = () => {
  // array destructing
  const [resource, setResource] = useState('posts');

  // object destructing => is going to look at the start state object. 
  // It would then pull off the resource property because we have the curly braces right
  // here and assign that resource value to the variable of resource.
  // const {resource} = this.state;

  
    return (
    <div>
      <div>
        <button onClick={() => setResource('posts')}>
          Posts  
        </button>
        <button onClick={() => setResource('todos')}>
          Todos
        </button>
      </div>
      {/* {resource} */}
      <ResourceList resource={resource} />
      {/* 
        - this part of the code resource={*resource*} => this variable right here is 
          our piece of component level state.
        - is going to always be a string of either posts or todos
      */}
    </div>
    );
}

export default App;